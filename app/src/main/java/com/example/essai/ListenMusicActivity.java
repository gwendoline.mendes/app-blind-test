package com.example.essai;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Random;

public class ListenMusicActivity extends AppCompatActivity {

    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listen_music);

        //récupération des variables globales de MainActivity et de la fileList
        int NbBonnesReponses = getIntent().getIntExtra("NbBonnesReponses",0);
        int coups = getIntent().getIntExtra("coups",0);
        ArrayList<String> fileList =  (ArrayList<String>)getIntent().getSerializableExtra("FILES_TO_SEND");

        //importation des musiques
        int[] sons = new int[] {
                R.raw.acdc,
                R.raw.adele,
                R.raw.angele,
                R.raw.annemarie,
                R.raw.arianna,
                R.raw.ayanakamura,
                R.raw.biggie,
                R.raw.booba,
                R.raw.charlieputh,
                R.raw.cole,
                R.raw.drake,
                R.raw.eagles,
                R.raw.edsheeran,
                R.raw.eminem,
                R.raw.fiftycent,
                R.raw.imaginedragons,
                R.raw.jdg,
                R.raw.kanye,
                R.raw.kendricklamar,
                R.raw.mj,
                R.raw.naruto,
                R.raw.ollymurs,
                R.raw.rihanna,
                R.raw.sinatra,
                R.raw.survivor,
        };

        // noms des musiques
        String[] nomMusiques = {
                "AC/DC",
                "Adele",
                "Angèle",
                "Anne-Marie",
                "Arianna Grande",
                "Aya Nakamura",
                "Biggie Smalls",
                "Booba",
                "Charlie Puth",
                "J.Cole",
                "Drake",
                "Eagles",
                "Ed Sheeran",
                "Eminem",
                "50 Cent",
                "Imagine Dragons",
                "Jean-Jaques Goldman",
                "Kanye West",
                "Kendrick Lamar",
                "Michael Jackson",
                "Naruto",
                "Olly Murs",
                "Rihanna",
                "Frank Sinatra",
                "Survivor",
        };


        // choix aléatoire d'une chanson de ce tableau
        Random random = new Random();
        final int nombreHasard = random.nextInt(sons.length-1);
        String nomChanson;

        //lancer la musique si elle n'a  pas déjà été lancée dans un coup précédent, sinon on prend la musique d'avant ou d'après
        if (fileList.contains(nomMusiques[nombreHasard])){
            if(nombreHasard==0){
                mediaPlayer =MediaPlayer.create(getApplicationContext(),sons[nombreHasard+1]);
                nomChanson = nomMusiques[nombreHasard+1];
                fileList.add(nomMusiques[nombreHasard+1]);
            }
            else{
                mediaPlayer =MediaPlayer.create(getApplicationContext(),sons[nombreHasard-1]);
                nomChanson = nomMusiques[nombreHasard-1];
                fileList.add(nomMusiques[nombreHasard-1]);
            }
        }
        else{
            mediaPlayer =MediaPlayer.create(getApplicationContext(),sons[nombreHasard]);
            nomChanson = nomMusiques[nombreHasard];
            fileList.add(nomChanson);
        }
        mediaPlayer.start();

        //si jamais l'utilisateur va au bout de la musique, on passe à BlindtestActivity
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                //créer l'action
                Intent intent = new Intent(getApplicationContext(),BlindtestActivity.class);
                intent.putExtra("NomChanson",nomChanson);
                intent.putExtra("NbBonnesReponses",NbBonnesReponses);
                intent.putExtra("coups",coups);
                intent.putExtra("FILES_TO_SEND", fileList);
                finish();
                startActivity(intent);
            }
        });

        //recuperation du bouton retour
        Button retour = findViewById(R.id.retour);
        //le clic renvoie à la MainActivity
        retour.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //on arrête la musique
                mediaPlayer.stop();
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }
        });

        //recuperation du bouton skip
        Button skip = findViewById(R.id.skip);
        //le clic renvoie à BlindtestActivity
        skip.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //on arrête la musique
                mediaPlayer.stop();
                Intent intent = new Intent(getApplicationContext(), BlindtestActivity.class);
                //transmission des variables globales à la prochaine activité
                intent.putExtra("NomChanson", nomChanson);
                intent.putExtra("NbBonnesReponses",NbBonnesReponses);
                intent.putExtra("coups",coups);
                intent.putExtra("FILES_TO_SEND", fileList);
                finish();
                startActivity(intent);
            }
        });

    }
}