package com.example.essai;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    //création des variables globales pour l'affichage du score
    private  int coups;
    private int NbBonnesReponses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //lancement de la musique d'acceuil
        MediaPlayer mediaPlayer =MediaPlayer.create(getApplicationContext(),R.raw.home);
        mediaPlayer.start();


        //recuperation du bouton jouer
        Button jouer = findViewById(R.id.jouer);
        //le clic sur le bouton renvoie à ListenMusicActivity
        jouer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //on arrête la musique
                mediaPlayer.stop();
                Intent versListenMusic = new Intent();
                versListenMusic.setClass(getApplicationContext(), ListenMusicActivity.class);
                //transmission des variables globales à la prochaine activité
                versListenMusic.putExtra("NbBonnesReponses",NbBonnesReponses);
                versListenMusic.putExtra("coups",coups);
                //création d'une liste qui contiendra les musiques jouées à chaque coup
                ArrayList<String> fileList = new ArrayList<>();
                versListenMusic.putExtra("FILES_TO_SEND", fileList);
                startActivity(versListenMusic);
                finish();
            }
        });


        //recupération du bouton learnmore
        Button more = findViewById(R.id.more);
        //le clic sur le bouton renvoie à LearnMoreActivity
        more.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //on arrête la musique
                mediaPlayer.stop();
                startActivity(new Intent(getApplicationContext(), LearnMoreActivity.class));
                finish();
            }
        });

    }

}