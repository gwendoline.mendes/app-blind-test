package com.example.essai;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        int nbReussites = getIntent().getIntExtra("NbBonnesReponses",0);
        String messageScore=String.format(getString(R.string.score), nbReussites);
        TextView tvScore = findViewById(R.id.textView_msgDuScore);
        tvScore.setText(messageScore);
    }
    public void rejouer(View view) {
        Intent versAccueil = new Intent();
        versAccueil.setClass(getApplicationContext(), MainActivity.class);
        startActivity(versAccueil);
        finish();
    }
}