package com.example.essai;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LearnMoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn_more);

        //lancer la musique de fin
        MediaPlayer mediaPlayer =MediaPlayer.create(getApplicationContext(),R.raw.giorno);
        mediaPlayer.start();

        //recuperer le bouton retour

        Button back = findViewById(R.id.back);
        // associer le bouton à un evenement
        back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finish();
                mediaPlayer.stop();

                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }

        });
    }
    public void wikipedia(View view) {
        Intent browserWikipedia=new Intent(Intent.ACTION_VIEW, Uri.parse("https://chartmasters.org/best-selling-artists-of-all-time/"));
        startActivity(browserWikipedia);}

    public void news(View view) {
        Intent browsernews=new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.rollingstone.com/music/music-news/"));
        startActivity(browsernews);}

    public void aboutus(View view) {
        Intent browseraboutus=new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.centralesupelec.fr/"));
        startActivity(browseraboutus);}
}