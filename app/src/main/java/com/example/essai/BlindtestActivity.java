package com.example.essai;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BlindtestActivity extends AppCompatActivity {

    //variables globales
    int NbBonnesReponses;
    int coups;
    ArrayList<String> fileList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blindtest);

        Random random = new Random();

        //récupération des variables de la ListenMusicActivity
        String nomChanson = getIntent().getStringExtra("NomChanson");
        NbBonnesReponses = getIntent().getIntExtra("NbBonnesReponses",0);
        coups = getIntent().getIntExtra("coups",0);
        fileList =  (ArrayList<String>)getIntent().getSerializableExtra("FILES_TO_SEND");

        // liste de fausses chansons
        List<String> faussesMusiques = new ArrayList<>();
        faussesMusiques.add("Beyonce");
        faussesMusiques.add("Justin Bieber");
        faussesMusiques.add("The Beatles");
        faussesMusiques.add("Stromae");
        faussesMusiques.add("Black Eye Peas");
        faussesMusiques.add("One Direction");
        faussesMusiques.add("Jacques Brel");
        faussesMusiques.add("Sexion d'assaut");
        faussesMusiques.add("Tokyo Hotel");
        faussesMusiques.add("Bruno Mars");
        faussesMusiques.add("Jul");
        faussesMusiques.add("John Legend");
        faussesMusiques.add("Hershey");
        faussesMusiques.add("Niska");
        faussesMusiques.add("PNL");
        faussesMusiques.add("Hatik");
        faussesMusiques.add("Meek Mill");
        faussesMusiques.add("Diam's");
        faussesMusiques.add("Luis Fonsi");
        faussesMusiques.add("Wejdene");
        faussesMusiques.add("U2");
        faussesMusiques.add("Shawn Mendes");
        faussesMusiques.add("Golden Experience");
        faussesMusiques.add("BTS");
        faussesMusiques.add("Queen");
        faussesMusiques.add("Prince");
        faussesMusiques.add("David Bowie");
        faussesMusiques.add("Elvis Presley");
        faussesMusiques.add("Charles Aznavour");
        faussesMusiques.add("Maitre Gim's");
        faussesMusiques.add("Black M");
        faussesMusiques.add("Tupac");
        faussesMusiques.add("Shakira");
        faussesMusiques.add("Renaud");
        faussesMusiques.add("Edith Piaf");
        faussesMusiques.add("Lady Gaga");
        faussesMusiques.add("Chris Brown");


        //création d'une liste qui contient les musiques déjà proposées pour éviter d'avoir des doublons
        ArrayList<String> propositions = new ArrayList<>();
        //récupération des boutons
        ConstraintLayout rootLayout = findViewById(R.id.réponses);
        int count = rootLayout.getChildCount();
        for(int i = 0; i<count;i++){
            //récupération du bouton i
            final Button button = (Button) rootLayout.getChildAt(i);
            //attribution d'une chanson random à ce bouton
            int randomNumber = random.nextInt(faussesMusiques.size()-1);
            String fausseMusiqueGenere;
            //si cette musique a déjà été proposée, on prend celle d'avant ou d'après
            if(propositions.contains(faussesMusiques.get(randomNumber))){
                if (randomNumber==0){
                    propositions.add(faussesMusiques.get(randomNumber+1));
                    fausseMusiqueGenere = faussesMusiques.get(randomNumber+1);
                }
                else {
                    propositions.add(faussesMusiques.get(randomNumber-1));
                    fausseMusiqueGenere = faussesMusiques.get(randomNumber-1);
                }
            }
            else{
                propositions.add(faussesMusiques.get(randomNumber));
                fausseMusiqueGenere = faussesMusiques.get(randomNumber);
            }
            button.setText(fausseMusiqueGenere);

            //si on clique sur un de ces faux boutons on envoie le msg "Perdu !"
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //afficher un toast
                    Toast.makeText(getApplicationContext(),"Perdu !",Toast.LENGTH_SHORT).show();
                    button.setEnabled(false);
                    prochaineMusique();

                }
            });
    }

        //choix d'un bouton au hasard pour afficher la bonne reponse (on écrase alors la mauvaise)
        int numeroBonneReponse = random.nextInt(count);
        final Button bonneReponseBtn = (Button) rootLayout.getChildAt(numeroBonneReponse);
        bonneReponseBtn.setText(nomChanson);
        //si on clique sur ce bouton on envoie le msg "Bravo Einstein"
        bonneReponseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // afficher un toast lors de la victoire
                bonneReponseBtn.setEnabled(false);
                NbBonnesReponses++;
                Toast.makeText(getApplicationContext(),"Bravo Einstein",Toast.LENGTH_SHORT).show();
                prochaineMusique();
            }
        });
}
public void prochaineMusique(){
        //attendre 2s
    coups++;
    Handler handler = new Handler();
    handler.postDelayed(new Runnable() {
        @Override
        public void run() {
            //redirige vers listenmusicactivity
            if (coups<10){
                Intent versListenMusic = new Intent();
                versListenMusic.setClass(getApplicationContext(),ListenMusicActivity.class);
                versListenMusic.putExtra("NbBonnesReponses",NbBonnesReponses);
                versListenMusic.putExtra("coups",coups);
                versListenMusic.putExtra("FILES_TO_SEND", fileList);
                startActivity(versListenMusic);
                finish();
            }
            else{
                Intent versScore = new Intent();
                versScore.setClass(getApplicationContext(),ScoreActivity.class);
                versScore.putExtra("NbBonnesReponses",NbBonnesReponses);
                startActivity(versScore);
                finish();
            }
        }
    },2000);





}
}